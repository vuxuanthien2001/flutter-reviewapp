import 'package:flutter/material.dart';
import 'package:flutter_appreview/screen/home_screen.dart';
import 'package:flutter_appreview/screen/welcome_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: WelComePage(),
    );
  }
}