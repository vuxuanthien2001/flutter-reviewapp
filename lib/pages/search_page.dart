import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_appreview/widgets/text_bold.dart';
import 'package:flutter_appreview/widgets/text_normal.dart';
import 'package:image_picker/image_picker.dart';

import '../misc/color.dart';
import '../widgets/app_icon.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          color: AppColors.mainColor,
      child: Column(
        children: [
          _mySearch(),
          SizedBox(height: 20,),
          Expanded(
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Container(
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30),
                    ),
                  ),
                padding: EdgeInsets.only(left: 20, right: 20),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 20,
                    ),
                    _myListNearbyAndOpenNow(),
                    SizedBox(
                      height: 20,
                    ),
                    _myListPicksForYou(),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    ));
  }
}

class _myListPicksForYou extends StatelessWidget {
  const _myListPicksForYou({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            TextBold(
              text: "Picks for you",
              size: 20,
            ),
          ],
        ),
        ListView.builder(
          shrinkWrap: true,
          primary: false,
          itemCount: 3,
          itemBuilder: (context, index) {
            return Card(
              color: AppColors.colorWhile,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: Container(
                child: Column(children: [
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        TextBold(
                          text: " A Restaurants",
                          size: 20,
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  Container(
                    padding: const EdgeInsets.only(left: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Wrap(
                          children: List.generate(5, (index) {
                            return Icon(Icons.star,
                                size: 24, color: AppColors.starColor);
                          }),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    padding: const EdgeInsets.only(
                        top: 10, left: 20, right: 20, bottom: 10),
                    height: 150,
                    child: Row(
                      children: [
                        Container(
                          width: 180,
                          height: 150,
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            image: DecorationImage(
                                image: NetworkImage(
                                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSYgv_7WcHGDMSb2j_ZbjdqXWr9s0UEumDMag&usqp=CAU"),
                                fit: BoxFit.cover),
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Stack(
                          children: [
                            Positioned(
                                child: Container(
                              width: 80,
                              height: 150,
                              decoration: const BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                                image: DecorationImage(
                                    image: NetworkImage(
                                        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR-Gcu0mBkr0uW_OPxSC_nlpdH2W7uTpqcZ6A&usqp=CAU"),
                                    fit: BoxFit.cover),
                              ),
                            )),
                            Positioned(
                                child: Container(
                                    width: 80,
                                    height: 150,
                                    decoration: BoxDecoration(
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(10)),
                                      color: Color.fromARGB(255, 0, 0, 0)
                                          .withOpacity(0.5),
                                    ),
                                    child: Container(
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          TextBold(
                                            text: "6",
                                            color: AppColors.buttonBackground,
                                          ),
                                          TextNormal(
                                              text: "more",
                                              color: AppColors.buttonBackground)
                                        ],
                                      ),
                                    ))),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 60,
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.all(Radius.circular(10)),
                      color: AppColors.colorCard1,
                    ),
                    margin: const EdgeInsets.only(left: 20, right: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          padding: const EdgeInsets.only(left: 20, top: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              TextBold(
                                text: "Category",
                                size: 12,
                                color: AppColors.textColor2,
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              TextNormal(
                                text: "Food & Drink",
                                color: AppColors.mainTextColor,
                                size: 15,
                              )
                            ],
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.only(right: 20, top: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              TextBold(
                                text: "Location",
                                size: 12,
                                color: AppColors.textColor2,
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              TextNormal(
                                text: "Secang Yogyakarta",
                                color: AppColors.mainTextColor,
                                size: 15,
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(top: 15, bottom: 20),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            padding: const EdgeInsets.only(left: 20, right: 20),
                            child: Column(
                              children: [
                                AppIcon(
                                    source: "assets/icon/like.svg",
                                    color: AppColors.mainTextColor),
                                const SizedBox(
                                  height: 5,
                                ),
                                TextNormal(
                                  text: "68",
                                  color: AppColors.mainTextColor,
                                )
                              ],
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.only(left: 20, right: 20),
                            child: Column(
                              children: [
                                AppIcon(
                                    source: "assets/icon/dislike.svg",
                                    color: AppColors.mainTextColor),
                                const SizedBox(
                                  height: 5,
                                ),
                                TextNormal(
                                  text: "0",
                                  color: AppColors.mainTextColor,
                                )
                              ],
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(left: 20, right: 20),
                            child: Column(
                              children: [
                                AppIcon(
                                    source: "assets/icon/chat.svg",
                                    color: AppColors.mainTextColor),
                                const SizedBox(
                                  height: 5,
                                ),
                                TextNormal(
                                  text: "8",
                                  color: AppColors.mainTextColor,
                                )
                              ],
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.only(left: 20, right: 20),
                            child: Column(
                              children: [
                                AppIcon(
                                    source: "assets/icon/share-option.svg",
                                    color: AppColors.mainTextColor),
                                const SizedBox(
                                  height: 5,
                                ),
                                TextNormal(
                                  text: "3",
                                  color: AppColors.mainTextColor,
                                )
                              ],
                            ),
                          ),
                        ]),
                  ),
                ]),
              ),
            );
          },
        ),
      ],
    );
  }
}

class _myListNearbyAndOpenNow extends StatelessWidget {
  const _myListNearbyAndOpenNow({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            TextBold(
              text: "Nearby and open now",
              size: 20,
            ),
          ],
        ),
        const SizedBox(
          height: 10,
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
              children: List.generate(
                  10,
                  (index) => Card(
                        color: AppColors.colorWhile,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: Container(
                            padding: EdgeInsets.all(10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      height: 100,
                                      width: 160,
                                      child: Image(
                                          image: NetworkImage(
                                              "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRRfpZB0_3qGRT0vx7Jlw662goIgQc9en4esg&usqp=CAU")),
                                    ),
                                    TextBold(
                                      text: "Bim Bim",
                                      size: 18,
                                    ),
                                    Wrap(
                                      children: List.generate(5, (index) {
                                        return Icon(
                                          Icons.star,
                                          color: AppColors.starColor,
                                          size: 12,
                                        );
                                      }),
                                    ),
                                    Row(
                                      children: [
                                        Icon(
                                          Icons.location_on,
                                          color: AppColors.colorLocation,
                                        ),
                                        TextNormal(text: "Hà Nội")
                                      ],
                                    )
                                  ],
                                ),
                              ],
                            )),
                      ))),
        ),
      ],
    );
  }
}

class _mySearch extends StatelessWidget {
  const _mySearch({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      padding: EdgeInsets.only(top: 50, left: 20, right: 20),
      child: const TextField(
        decoration: InputDecoration(
          fillColor: Colors.white,
          hintText: "Search for burgers, delivery, barbers on ...",
          filled: true,
          prefixIcon: Icon(Icons.search),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(15.0),
            ),
          ),
        ),
      ),
    );
  }
}
