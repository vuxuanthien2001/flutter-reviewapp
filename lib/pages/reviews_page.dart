import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_appreview/screen/detail_screen.dart';
import 'package:flutter_appreview/misc/color.dart';
import 'package:flutter_appreview/widgets/app_icon.dart';
import 'package:flutter_appreview/widgets/text_bold.dart';
import 'package:flutter_appreview/widgets/text_normal.dart';
import 'package:flutter_svg/svg.dart';

class ReviewsPage extends StatefulWidget {
  const ReviewsPage({Key? key}) : super(key: key);

  @override
  State<ReviewsPage> createState() => _ReviewsPageState();
}

class _ReviewsPageState extends State<ReviewsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: AppColors.mainColor,
        child: Column(
          children: [
          _myTitle(),
          Expanded(
            child: ListView.builder(
              itemCount: 3,
              itemBuilder: (context, index) {
                return _myPosts(context);
              },
            ),
          )
        ]),
      ),
    );
  }

  Widget _myPosts(BuildContext context) {
    return Column(
                children: [
                  Container(
                    padding: const EdgeInsets.only(left: 20, right: 20),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(50)),
                    ),
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const DetailPage(),
                          ),
                        );
                      },
                      child: Card(
                        color: AppColors.colorWhile,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: Column(children: [
                          Container(
                            padding: const EdgeInsets.only(
                                left: 10, right: 10, top: 10),
                            child: Row(
                              // ignore: prefer_const_literals_to_create_immutables
                              children: [
                                const SizedBox(
                                  height: 50,
                                  width: 50,
                                  child: CircleAvatar(
                                      backgroundImage: NetworkImage(
                                          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR4n0ddX3HxaKRtCaF-kOHrOALtmjJgc4--qA&usqp=CAU")),
                                ),
                                Expanded(
                                    child: Container(
                                  child: Row(
                                    children: [
                                      const SizedBox(
                                        width: 8,
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          TextBold(
                                            text: "Carolyne Agustine",
                                            size: 15,
                                          ),
                                          TextNormal(text: "25 minutes ago")
                                        ],
                                      ),
                                    ],
                                  ),
                                )),

                                AppIcon(source: "assets/icon/more.svg", color: AppColors.mainTextColor)
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Container(
                            width: double.maxFinite,
                            height: 1,
                            color: AppColors.colorWhile2,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          TextBold(text: "Secangkir Jawa", size: 20,),
                          const SizedBox(
                            height: 5,
                          ),
                          Wrap(
                            children: List.generate(5, (index) {
                              return Icon(Icons.star, size: 32,
                                  color: AppColors.starColor);
                            }),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Container(
                            padding:
                                const EdgeInsets.only(left: 20, right: 20),
                            child: Column(
                              // ignore: prefer_const_literals_to_create_immutables
                              children: [
                                const Text.rich(TextSpan(
                                    text:
                                        "I'm very happy with the services. I think this is the best cafe in Yogyakar ...",
                                    children: <InlineSpan>[
                                      TextSpan(
                                        text: " See more",
                                        style: TextStyle(color: Colors.grey),
                                      )
                                    ])),
                              ],
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.only(
                                top: 10, left: 20, right: 20, bottom: 10),
                            height: 150,
                            child: Row(
                              children: [
                                Container(
                                  width: 180,
                                  height: 150,
                                  decoration: const BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10)),
                                    image: DecorationImage(
                                        image: NetworkImage(
                                            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSYgv_7WcHGDMSb2j_ZbjdqXWr9s0UEumDMag&usqp=CAU"),
                                        fit: BoxFit.cover),
                                  ),
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                Stack(
                                  children: [
                                    Positioned(
                                        child: Container(
                                      width: 80,
                                      height: 150,
                                      decoration: const BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10)),
                                        image: DecorationImage(
                                            image: NetworkImage(
                                                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR-Gcu0mBkr0uW_OPxSC_nlpdH2W7uTpqcZ6A&usqp=CAU"),
                                            fit: BoxFit.cover),
                                      ),
                                    )),
                                    Positioned(
                                        child: Container(
                                            width: 80,
                                            height: 150,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  const BorderRadius.all(
                                                      Radius.circular(10)),
                                              color:
                                                  Color.fromARGB(255, 0, 0, 0)
                                                      .withOpacity(0.5),
                                            ),
                                            child: Container(
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  TextBold(
                                                    text: "6",
                                                    color: AppColors
                                                        .buttonBackground,
                                                  ),
                                                  TextNormal(
                                                      text: "more",
                                                      color: AppColors
                                                          .buttonBackground)
                                                ],
                                              ),
                                            ))),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Container(
                            height: 60,
                            decoration: BoxDecoration(
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(10)),
                              color: AppColors.colorCard1,
                            ),
                            margin:
                                const EdgeInsets.only(left: 20, right: 20),
                            child: Row(
                              mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  padding: const EdgeInsets.only(
                                      left: 20, top: 10),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      TextBold(
                                        text: "Category",
                                        size: 12,
                                        color: AppColors.textColor2,
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      TextNormal(
                                        text: "Food & Drink",
                                        color: AppColors.mainTextColor,
                                        size: 15,
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  padding: const EdgeInsets.only(
                                      right: 20, top: 10),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      TextBold(
                                        text: "Location",
                                        size: 12,
                                        color: AppColors.textColor2,
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      TextNormal(
                                        text: "Secang Yogyakarta",
                                        color: AppColors.mainTextColor,
                                        size: 15,
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            padding:
                                const EdgeInsets.only(top: 15, bottom: 20),
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    padding: const EdgeInsets.only(
                                        left: 20, right: 20),
                                    child: Column(
                                      children: [
                                        AppIcon(source: "assets/icon/like.svg", color: AppColors.mainTextColor),
                                        const SizedBox(
                                          height: 5,
                                        ),
                                        TextNormal(
                                          text: "68",
                                          color: AppColors.mainTextColor,
                                        )
                                      ],
                                    ),
                                  ),
                                  Container(
                                    padding: const EdgeInsets.only(
                                        left: 20, right: 20),
                                    child: Column(
                                      children: [
                                        AppIcon(source: "assets/icon/dislike.svg", color: AppColors.mainTextColor),
                                        const SizedBox(
                                          height: 5,
                                        ),
                                        TextNormal(
                                          text: "0",
                                          color: AppColors.mainTextColor,
                                        )
                                      ],
                                    ),
                                  ),
                                  Container(
                                    padding:
                                        EdgeInsets.only(left: 20, right: 20),
                                    child: Column(
                                      children: [
                                        AppIcon(source: "assets/icon/chat.svg", color: AppColors.mainTextColor),
                                        const SizedBox(
                                          height: 5,
                                        ),
                                        TextNormal(
                                          text: "8",
                                          color: AppColors.mainTextColor,
                                        )
                                      ],
                                    ),
                                  ),
                                  Container(
                                    padding:
                                        const EdgeInsets.only(left: 20, right: 20),
                                    child: Column(
                                      children: [
                                        
                                        AppIcon(source: "assets/icon/share-option.svg", color: AppColors.mainTextColor),
                                        const SizedBox(
                                          height: 5,
                                        ),
                                        TextNormal(
                                          text: "3",
                                          color: AppColors.mainTextColor,
                                        )
                                      ],
                                    ),
                                  ),
                                ]),
                          ),
                        ]),
                      ),
                    ),
                  ),
                ],
              );
  }

  Widget _myTitle() {
    return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              padding: const EdgeInsets.only(left: 20, top: 50),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextBold(
                    text: "Review",
                    color: AppColors.buttonBackground,
                  ),
                  TextNormal(
                    text: "Browse any reviews for your reference",
                    color: AppColors.mainTextColor,
                  )
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.only(right: 20, top: 50),
              child: SizedBox(
                height: 30,
                width: 30,
                child: SvgPicture.asset("assets/icon/menu.svg",
                    color: AppColors.mainTextColor),
              ),
            ),
          ],
        );
  }
}
