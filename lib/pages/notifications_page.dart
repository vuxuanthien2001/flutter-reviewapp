import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_svg/svg.dart';

import '../misc/color.dart';
import '../widgets/text_bold.dart';
import '../widgets/text_normal.dart';

class NotificationPage extends StatefulWidget {
  const NotificationPage({Key? key}) : super(key: key);

  @override
  State<NotificationPage> createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: AppColors.mainColor,
        child: Column(
          children: [
            _titleNotification(context),
            const SizedBox(
              height: 10,
            ),
            Expanded(
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Container(
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30),
                    ),
                  ),
                  child: Column(
                    children: [
                      _myJustNow(),
                    
                      _myYesterday(),

                      _myThisWeek()
                    
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _myThisWeek() {
    return Container(
                      width: double.maxFinite,
                      padding:
                          const EdgeInsets.only(left: 15, top: 15),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: const EdgeInsets.only(bottom: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                TextBold(
                            text: "THIS WEEK",
                            size: 15,
                            color: AppColors.mainTextColor,
                          ),

                          ListView.builder(
                            shrinkWrap: true,
                            primary: false,
                            itemCount: 5,
                            itemBuilder: (context, index) {
                              return Column(
                                
                                children: [
                                  Row(
                                    children: [
                                      Stack(
                                        children: [
                                          const Positioned(
                                            child: SizedBox(
                                              height: 50,
                                              width: 50,
                                              child: CircleAvatar(
                                                  backgroundImage: NetworkImage(
                                                      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4KLQr4UnxPHkhfpKkU6bY_04vGtGzTKB_pg&usqp=CAU")),
                                            ),
                                          ),
                                          Positioned(
                                            right: 0,
                                            bottom: 0,
                                            child: Container(
                                              padding: const EdgeInsets.only(
                                                  top: 5,
                                                  right: 5,
                                                  left: 5,
                                                  bottom: 5),
                                              height: 25,
                                              width: 25,
                                              decoration: BoxDecoration(
                                                  color: AppColors.colorWhile2,
                                                  shape: BoxShape.circle),
                                              child: SvgPicture.asset(
                                                  "assets/icon/chat.svg",
                                                  color: AppColors.colorClickBottom),
                                            ),
                                          )
                                        ],
                                      ),
                                      const SizedBox(
                                        width: 5,
                                      ),
                                      Row(
                                        children: [
                                          const SizedBox(
                                            width: 5,
                                          ),
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text.rich(TextSpan(
                                                  text: "Loretta Fleming",
                                                  style: const TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold),
                                                  children: <InlineSpan>[
                                                    TextSpan(
                                                      text: " mentioned you in a comment",
                                                      style: DefaultTextStyle.of(
                                                              context)
                                                          .style,
                                                    )
                                                  ])),
                                              TextNormal(text: "Mar 30")
                                            ],
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  const SizedBox(height: 5,)
                                ],
                              );
                            },
                          )
                              ],
                            ),
                          )
                        ],
                      ),
                    );
  }

  Widget _myYesterday() {
    return Container(
                      width: double.maxFinite,
                      padding:
                          const EdgeInsets.only(left: 15, right: 15, top: 15),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: EdgeInsets.only(bottom: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                TextBold(
                            text: "YESTERDAY",
                            size: 15,
                            color: AppColors.mainTextColor,
                          ),

                          ListView.builder(
                            shrinkWrap: true,
                            primary: false,
                            itemCount: 3,
                            itemBuilder: (context, index) {
                              return Column(
                                
                                children: [
                                  Row(
                                    children: [
                                      Stack(
                                        children: [
                                          const Positioned(
                                            child: SizedBox(
                                              height: 50,
                                              width: 50,
                                              child: CircleAvatar(
                                                  backgroundImage: NetworkImage(
                                                      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4KLQr4UnxPHkhfpKkU6bY_04vGtGzTKB_pg&usqp=CAU")),
                                            ),
                                          ),
                                          Positioned(
                                            right: 0,
                                            bottom: 0,
                                            child: Container(
                                              padding: const EdgeInsets.only(
                                                  top: 5,
                                                  right: 5,
                                                  left: 5,
                                                  bottom: 5),
                                              height: 25,
                                              width: 25,
                                              decoration: BoxDecoration(
                                                  color: AppColors.colorWhile2,
                                                  shape: BoxShape.circle),
                                              child: SvgPicture.asset(
                                                  "assets/icon/dislike.svg",
                                                  color: AppColors.bigTextColor),
                                            ),
                                          )
                                        ],
                                      ),
                                      const SizedBox(
                                        width: 5,
                                      ),
                                      Row(
                                        children: [
                                          const SizedBox(
                                            width: 5,
                                          ),
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text.rich(TextSpan(
                                                  text: "Loretta Fleming",
                                                  style: const TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold),
                                                  children: <InlineSpan>[
                                                    TextSpan(
                                                      text: " dislike your review",
                                                      style: DefaultTextStyle.of(
                                                              context)
                                                          .style,
                                                    )
                                                  ])),
                                              TextNormal(text: "1 day ago")
                                            ],
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  const SizedBox(height: 5,)
                                ],
                              );
                            },
                          )
                              ],
                            ),
                          )
                        ],
                      ),
                    );
  }

  Widget _myJustNow() {
    return Container(
                      width: double.maxFinite,
                      padding:
                          const EdgeInsets.only(left: 15, right: 15, top: 15),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: EdgeInsets.only(bottom: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                TextBold(
                            text: "JUST NOW",
                            size: 15,
                            color: AppColors.mainTextColor,
                          ),

                          ListView.builder(
                            shrinkWrap: true,
                            primary: false,
                            itemCount: 2,
                            itemBuilder: (context, index) {
                              return Column(
                                
                                children: [
                                  Row(
                                    children: [
                                      Stack(
                                        children: [
                                          const Positioned(
                                            child: SizedBox(
                                              height: 50,
                                              width: 50,
                                              child: CircleAvatar(
                                                  backgroundImage: NetworkImage(
                                                      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4KLQr4UnxPHkhfpKkU6bY_04vGtGzTKB_pg&usqp=CAU")),
                                            ),
                                          ),
                                          Positioned(
                                            right: 0,
                                            bottom: 0,
                                            child: Container(
                                              padding: const EdgeInsets.only(
                                                  top: 5,
                                                  right: 5,
                                                  left: 5,
                                                  bottom: 5),
                                              height: 25,
                                              width: 25,
                                              decoration: BoxDecoration(
                                                  color: AppColors.colorWhile2,
                                                  shape: BoxShape.circle),
                                              child: SvgPicture.asset(
                                                  "assets/icon/like.svg",
                                                  color: AppColors.mainColor),
                                            ),
                                          )
                                        ],
                                      ),
                                      const SizedBox(
                                        width: 5,
                                      ),
                                      Row(
                                        children: [
                                          const SizedBox(
                                            width: 5,
                                          ),
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text.rich(TextSpan(
                                                  text: "Loretta Fleming",
                                                  style: const TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold),
                                                  children: <InlineSpan>[
                                                    TextSpan(
                                                      text: " like your review",
                                                      style: DefaultTextStyle.of(
                                                              context)
                                                          .style,
                                                    )
                                                  ])),
                                              TextNormal(text: "Just now")
                                            ],
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 5,)
                                ],
                              );
                            },
                          )
                              ],
                            ),
                          )
                        ],
                      ),
                    );
  }

  Widget _titleNotification(BuildContext context) {
    return Container(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 50),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TextBold(
                  text: "Notification",
                  color: AppColors.buttonBackground,
                ),
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: SizedBox(
                    height: 24,
                    width: 24,
                    child: SvgPicture.asset("assets/icon/more.svg",
                        color: AppColors.colorWhile2),
                  ),
                ),
              ],
            ),
          );
  }
}
