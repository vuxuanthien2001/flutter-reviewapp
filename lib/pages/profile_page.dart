import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_appreview/widgets/app_icon.dart';
import 'package:flutter_svg/svg.dart';

import '../misc/color.dart';
import '../widgets/text_bold.dart';
import '../widgets/text_normal.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: AppColors.mainColor,
        child: Stack(
          children: [
            _myBackground(),
            _updateInformation(),
            _myAvatar(),
            Positioned(
              top: 200,
              child: Container(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                width: MediaQuery.of(context).size.width,
                height: 500,
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30))),
                child: Column(
                  children: [
                    Expanded(
                        child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(
                        children: [
                          _myName(),
                          Container(
                            child: Column(
                              children: [
                                const SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    TextNormal(
                                        text:
                                            "Het there! I'm using this app for rate"),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    TextNormal(text: "& review anything!"),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          _myInformationFollow(),
                          _myReviews(),
                          _myFollowers(),
                          _myFollowing(),
                          _myMedia(),
                          const SizedBox(
                            height: 50,
                          ),
                        ],
                      ),
                    )),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _myMedia() {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            child: Row(
              children: [
                TextBold(
                  text: "Media",
                  size: 20,
                ),
                Expanded(child: Container()),
                TextNormal(
                  text: "See All",
                  color: AppColors.mainColor,
                )
              ],
            ),
          ),
          Card(
            color: AppColors.colorWhile,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            child: Row(
              children: [
                Container(
                  margin: const EdgeInsets.only(left: 10),
                  height: 120,
                  width: 120,
                  decoration: const BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(
                            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ8yMIHkxs901EzRfpvMq6gIEfDADZcfbJxLw&usqp=CAU")),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(left: 10),
                  height: 120,
                  width: 120,
                  decoration: const BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(
                            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ8yMIHkxs901EzRfpvMq6gIEfDADZcfbJxLw&usqp=CAU")),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _myFollowing() {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            child: Row(
              children: [
                TextBold(
                  text: "My Following",
                  size: 20,
                ),
                TextNormal(
                  text: " (382)",
                  size: 18,
                ),
                Expanded(child: Container()),
                TextNormal(
                  text: "See All",
                  color: AppColors.mainColor,
                )
              ],
            ),
          ),
          Card(
            color: AppColors.colorWhile,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            child: Row(
              children: [
                Container(
                  padding: const EdgeInsets.only(
                      left: 10, top: 15, right: 10, bottom: 15),
                  child: Column(
                    // ignore: prefer_const_literals_to_create_immutables
                    children: [
                      const SizedBox(
                        height: 50,
                        width: 50,
                        child: CircleAvatar(
                          backgroundImage: NetworkImage(
                              "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQyv04EkH6F3uKG4ygZR6DmxQWvG3pIc_XEJw&usqp=CAU"),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      TextNormal(text: "Alexander")
                    ],
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(
                      left: 10, top: 15, right: 10, bottom: 15),
                  child: Column(
                    // ignore: prefer_const_literals_to_create_immutables
                    children: [
                      const SizedBox(
                        height: 50,
                        width: 50,
                        child: CircleAvatar(
                          backgroundImage: NetworkImage(
                              "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQyv04EkH6F3uKG4ygZR6DmxQWvG3pIc_XEJw&usqp=CAU"),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      TextNormal(text: "Alexander")
                    ],
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(
                      left: 10, top: 15, right: 10, bottom: 15),
                  child: Column(
                    // ignore: prefer_const_literals_to_create_immutables
                    children: [
                      const SizedBox(
                        height: 50,
                        width: 50,
                        child: CircleAvatar(
                          backgroundImage: NetworkImage(
                              "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQyv04EkH6F3uKG4ygZR6DmxQWvG3pIc_XEJw&usqp=CAU"),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      TextNormal(text: "Alexander")
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _myFollowers() {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            child: Row(
              children: [
                TextBold(
                  text: "My Followers",
                  size: 20,
                ),
                TextNormal(
                  text: " (382)",
                  size: 18,
                ),
                Expanded(child: Container()),
                TextNormal(
                  text: "See All",
                  color: AppColors.mainColor,
                )
              ],
            ),
          ),
          Card(
            color: AppColors.colorWhile,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            child: Row(
              children: [
                Container(
                  padding: const EdgeInsets.only(
                      left: 10, top: 15, right: 10, bottom: 15),
                  child: Column(
                    // ignore: prefer_const_literals_to_create_immutables
                    children: [
                      const SizedBox(
                        height: 50,
                        width: 50,
                        child: CircleAvatar(
                          backgroundImage: NetworkImage(
                              "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4KLQr4UnxPHkhfpKkU6bY_04vGtGzTKB_pg&usqp=CAU"),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      TextNormal(text: "Alexander")
                    ],
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(
                      left: 10, top: 15, right: 10, bottom: 15),
                  child: Column(
                    // ignore: prefer_const_literals_to_create_immutables
                    children: [
                      const SizedBox(
                        height: 50,
                        width: 50,
                        child: CircleAvatar(
                          backgroundImage: NetworkImage(
                              "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4KLQr4UnxPHkhfpKkU6bY_04vGtGzTKB_pg&usqp=CAU"),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      TextNormal(text: "Alexander")
                    ],
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(
                      left: 10, top: 15, right: 10, bottom: 15),
                  child: Column(
                    // ignore: prefer_const_literals_to_create_immutables
                    children: [
                      const SizedBox(
                        height: 50,
                        width: 50,
                        child: CircleAvatar(
                          backgroundImage: NetworkImage(
                              "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4KLQr4UnxPHkhfpKkU6bY_04vGtGzTKB_pg&usqp=CAU"),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      TextNormal(text: "Alexander")
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _myReviews() {
    return Container(
      padding: const EdgeInsets.only(top: 30),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            child: Row(
              children: [
                TextBold(
                  text: "My Reviews",
                  size: 20,
                ),
                TextNormal(
                  text: " (683)",
                  size: 18,
                ),
                Expanded(child: Container()),
                TextNormal(
                  text: "See All",
                  color: AppColors.mainColor,
                )
              ],
            ),
          ),
          Container(
            child: Card(
              color: AppColors.colorWhile,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: Column(children: [
                Container(
                  padding: const EdgeInsets.only(left: 10, right: 10, top: 10),
                  child: Row(
                    // ignore: prefer_const_literals_to_create_immutables
                    children: [
                      const SizedBox(
                        height: 50,
                        width: 50,
                        child: CircleAvatar(
                            backgroundImage: NetworkImage(
                                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR4n0ddX3HxaKRtCaF-kOHrOALtmjJgc4--qA&usqp=CAU")),
                      ),
                      Expanded(
                          child: Container(
                        child: Row(
                          children: [
                            const SizedBox(
                              width: 8,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                TextBold(
                                  text: "Carolyne Agustine",
                                  size: 15,
                                ),
                                TextNormal(text: "25 minutes ago")
                              ],
                            ),
                          ],
                        ),
                      )),
                      SizedBox(
                        height: 24,
                        width: 24,
                        child: SvgPicture.asset("assets/icon/more.svg",
                            color: AppColors.mainTextColor),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                Container(
                  width: double.maxFinite,
                  height: 1,
                  color: AppColors.colorWhile2,
                ),
                const SizedBox(
                  height: 10,
                ),
                TextBold(text: "Secangkir Jawa"),
                const SizedBox(
                  height: 5,
                ),
                Wrap(
                  children: List.generate(5, (index) {
                    return Icon(Icons.star, color: AppColors.starColor);
                  }),
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Column(
                    // ignore: prefer_const_literals_to_create_immutables
                    children: [
                      const Text.rich(TextSpan(
                          text:
                              "I'm very happy with the services. I think this is the best cafe in Yogyakar ...",
                          children: <InlineSpan>[
                            TextSpan(
                              text: " See more",
                              style: TextStyle(color: Colors.grey),
                            )
                          ])),
                    ],
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(
                      top: 10, left: 20, right: 20, bottom: 10),
                  height: 150,
                  child: Row(
                    children: [
                      Container(
                        width: 180,
                        height: 150,
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          image: DecorationImage(
                              image: NetworkImage(
                                  "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSYgv_7WcHGDMSb2j_ZbjdqXWr9s0UEumDMag&usqp=CAU"),
                              fit: BoxFit.cover),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Stack(
                        children: [
                          Positioned(
                              child: Container(
                            width: 80,
                            height: 150,
                            decoration: const BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              image: DecorationImage(
                                  image: NetworkImage(
                                      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR-Gcu0mBkr0uW_OPxSC_nlpdH2W7uTpqcZ6A&usqp=CAU"),
                                  fit: BoxFit.cover),
                            ),
                          )),
                          Positioned(
                              child: Container(
                                  width: 80,
                                  height: 150,
                                  decoration: BoxDecoration(
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(10)),
                                    color: Color.fromARGB(255, 0, 0, 0)
                                        .withOpacity(0.5),
                                  ),
                                  child: Container(
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        TextBold(
                                          text: "6",
                                          color: AppColors.buttonBackground,
                                        ),
                                        TextNormal(
                                            text: "more",
                                            color: AppColors.buttonBackground)
                                      ],
                                    ),
                                  ))),
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 60,
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.all(Radius.circular(10)),
                    color: AppColors.colorCard1,
                  ),
                  margin: const EdgeInsets.only(left: 20, right: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        padding: const EdgeInsets.only(left: 20, top: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            TextBold(
                              text: "Category",
                              size: 12,
                              color: AppColors.textColor2,
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            TextNormal(
                              text: "Food & Drink",
                              color: AppColors.mainTextColor,
                              size: 15,
                            )
                          ],
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(right: 20, top: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            TextBold(
                              text: "Location",
                              size: 12,
                              color: AppColors.textColor2,
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            TextNormal(
                              text: "Secang Yogyakarta",
                              color: AppColors.mainTextColor,
                              size: 15,
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(top: 15, bottom: 20),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          padding: const EdgeInsets.only(left: 20, right: 20),
                          child: Column(
                            children: [
                              SizedBox(
                                height: 24,
                                width: 24,
                                child: SvgPicture.asset("assets/icon/like.svg",
                                    color: AppColors.mainTextColor),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              TextNormal(
                                text: "68",
                                color: AppColors.mainTextColor,
                              )
                            ],
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.only(left: 20, right: 20),
                          child: Column(
                            children: [
                              SizedBox(
                                height: 24,
                                width: 24,
                                child: SvgPicture.asset(
                                    "assets/icon/dislike.svg",
                                    color: AppColors.mainTextColor),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              TextNormal(
                                text: "0",
                                color: AppColors.mainTextColor,
                              )
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 20, right: 20),
                          child: Column(
                            children: [
                              SizedBox(
                                height: 24,
                                width: 24,
                                child: SvgPicture.asset("assets/icon/chat.svg",
                                    color: AppColors.mainTextColor),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              TextNormal(
                                text: "8",
                                color: AppColors.mainTextColor,
                              )
                            ],
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.only(left: 20, right: 20),
                          child: Column(
                            children: [
                              SizedBox(
                                height: 24,
                                width: 24,
                                child: SvgPicture.asset(
                                    "assets/icon/share-option.svg",
                                    color: AppColors.mainTextColor),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              TextNormal(
                                text: "3",
                                color: AppColors.mainTextColor,
                              )
                            ],
                          ),
                        ),
                      ]),
                ),
              ]),
            ),
          ),
        ],
      ),
    );
  }

  Widget _myInformationFollow() {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            padding: EdgeInsets.only(left: 30),
            child: Column(
              children: [
                TextBold(text: "386"),
                TextNormal(text: "Followers")
              ],
            ),
          ),
          Container(
            height: 50,
            width: 1,
            color: AppColors.mainTextColor,
          ),
          Container(
            padding: EdgeInsets.only(right: 30),
            child: Column(
              children: [
                TextBold(text: "364"),
                TextNormal(text: "Following")
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _myName() {
    return Container(
      child: TextBold(text: "Carolyne Agustine"),
    );
  }

  Widget _myAvatar() {
    return Positioned(
        top: 100,
        right: 140,
        child: Container(
          height: 64,
          width: 64,
          decoration: BoxDecoration(
            border: Border.all(
              color: AppColors.colorWhile,
              width: 2,
            ),
            borderRadius: BorderRadius.circular(30),
            image: const DecorationImage(
                image: NetworkImage(
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQyv04EkH6F3uKG4ygZR6DmxQWvG3pIc_XEJw&usqp=CAU")),
          ),
        ));
  }

  Widget _updateInformation() {
    return Positioned(
      top: 30,
      right: 20,
      child: AppIcon(
        source: "assets/icon/edit.svg",
        height: 32,
        width: 32,
        color: AppColors.colorWhile,
      ),
    );
  }

  Widget _myBackground() {
    return Positioned(
        left: 0,
        right: 0,
        child: Container(
          height: 250,
          width: double.maxFinite,
          decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/image/nen.jpg"),
                  fit: BoxFit.cover)),
        ));
  }
}
