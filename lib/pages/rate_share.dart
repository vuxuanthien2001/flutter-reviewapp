import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_appreview/Controller/test_controller.dart';
import 'package:flutter_appreview/screen/google_map.dart';
import 'package:flutter_appreview/widgets/app_icon.dart';
import 'package:flutter_appreview/widgets/text_bold.dart';
import 'package:flutter_appreview/widgets/text_normal.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:video_player/video_player.dart';

import '../misc/color.dart';
import '../widgets/app_button.dart';

class RateSharePage extends StatefulWidget {
  const RateSharePage({Key? key}) : super(key: key);

  @override
  State<RateSharePage> createState() => _RateSharePageState();
}

class _RateSharePageState extends State<RateSharePage> {
  File? image;
  Future pickImage(ImageSource source) async {
    try {
      final image = await ImagePicker().pickImage(source: source);
      if (image == null) {
        return;
      }
      final imageTemporary = File(image.path);
      setState(() {
        this.image = imageTemporary;
      });

      print("Path ${image.path}");
    } on PlatformException catch (e) {
      print("Failed to pick image: $e");
    }
  }

  VideoPlayerController? controller;
  File? _video;
  final picker = ImagePicker();

  pickVideo() async {
    final video = await picker.getVideo(source: ImageSource.gallery);

    _video = File(video!.path);

    controller = VideoPlayerController.file(_video!)
      ..initialize().then((_) {
        setState(() {});

        controller!.play();
      });
  }

  String? _dropDownValue;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: AppColors.mainColor,
        child: Column(
          children: [
            _myTitleRate_Share(context),
            const SizedBox(
              height: 10,
            ),
            Expanded(
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Container(
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30),
                    ),
                  ),
                  child: Column(
                    children: [
                      _myReviewTitle(),
                      _myRate(),
                      _selectCategory(),
                      _myLocation(),
                      _myComment(),
                      _uploadPhotos(),
                      _uploadVideos(),
                      _mySubmit(),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _mySubmit() {
    return Container(
      padding: const EdgeInsets.only(left: 15, right: 15),
      child: ButtonApp(
        text: "Submit Your Review",
        textColor: AppColors.colorWhile,
        background: AppColors.mainColor,
        textSize: 20,
      ),
    );
  }

  Widget _uploadVideos() {
    return Container(
      width: double.maxFinite,
      padding: const EdgeInsets.only(left: 15, right: 15, top: 10, bottom: 10),
      child: InkWell(
        onTap: () {
          pickVideo();
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextBold(
              text: "Upload Videos",
              size: 20,
            ),
            const SizedBox(
              height: 10,
            ),
            DottedBorder(
              strokeWidth: 1,
              borderType: BorderType.RRect,
              radius: Radius.circular(12),
              padding: EdgeInsets.all(6),
              dashPattern: const [5, 5],
              color: AppColors.colorUnClickBottom,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  _video != null
                      ? Container(
                        width: 300,
                        height: 500,
                        child: (controller!.value.isInitialized
                          ? AspectRatio(
                              aspectRatio: 3/2,
                              child: VideoPlayer(controller!),
                            )
                          : Container(
                           
                          )),)
                      : Column(
                          // ignore: prefer_const_literals_to_create_immutables
                          children: [
                            const SizedBox(
                              height: 10,
                            ),
                            AppIcon(
                                source: "assets/icon/camera.svg",
                                color: AppColors.mainTextColor),
                            const SizedBox(
                              height: 10,
                            ),
                            TextNormal(
                              text: "Click to add video",
                              color: AppColors.mainTextColor,
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                          ],
                        ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _uploadPhotos() {
    return Container(
      width: double.maxFinite,
      padding: const EdgeInsets.only(left: 15, right: 15, top: 10, bottom: 10),
      child: InkWell(
        onTap: () {
          pickImage(ImageSource.gallery);
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextBold(
              text: "Upload Photos",
              size: 20,
            ),
            const SizedBox(
              height: 10,
            ),
            DottedBorder(
              strokeWidth: 1,
              borderType: BorderType.RRect,
              radius: Radius.circular(12),
              padding: EdgeInsets.all(6),
              dashPattern: const [5, 5],
              color: AppColors.colorUnClickBottom,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  image != null
                      ? Image.file(
                          image!,
                          width: 300,
                          fit: BoxFit.fitHeight,
                        )
                      : Column(
                          // ignore: prefer_const_literals_to_create_immutables
                          children: [
                            const SizedBox(
                              height: 10,
                            ),
                            AppIcon(
                                source: "assets/icon/add-image.svg",
                                color: AppColors.mainTextColor),
                            const SizedBox(
                              height: 10,
                            ),
                            TextNormal(
                              text: "Click to add photo",
                              color: AppColors.mainTextColor,
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                          ],
                        ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _myComment() {
    return Container(
      width: double.maxFinite,
      padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextBold(
            text: "Your Comment",
            size: 20,
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            decoration: BoxDecoration(
              border: Border.all(width: 1.0, color: AppColors.mainTextColor),
              borderRadius: BorderRadius.circular(20),
            ),
            child: const TextField(
              style: TextStyle(
                fontSize: 16,
              ),
              maxLines: 5,
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: "Write your comment here ..."),
            ),
          ),
        ],
      ),
    );
  }

  Widget _myLocation() {
    return Container(
      width: double.maxFinite,
      padding: const EdgeInsets.only(left: 15, right: 15, top: 10, bottom: 10),
      child: InkWell(
        onTap: (() {
          Navigator.push(context,
              MaterialPageRoute(builder: ((context) => Location())));
        }),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextBold(
              text: "Location",
              size: 20,
            ),
            const SizedBox(
              height: 10,
            ),
            DottedBorder(
              strokeWidth: 1,
              borderType: BorderType.RRect,
              radius: Radius.circular(12),
              padding: EdgeInsets.all(6),
              dashPattern: const [5, 5],
              color: AppColors.colorUnClickBottom,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    // ignore: prefer_const_literals_to_create_immutables
                    children: [
                      const SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.location_on,
                        color: AppColors.mainTextColor,
                        size: 32,
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      TextNormal(
                        text: "Choose the location if you want",
                        color: AppColors.mainTextColor,
                      ),
                      TextNormal(
                        text: "to review it specifically",
                        color: AppColors.mainTextColor,
                      ),
                      const SizedBox(
                        height: 24,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _selectCategory() {
    return Container(
      width: double.maxFinite,
      padding: const EdgeInsets.only(left: 15, right: 15, top: 10, bottom: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextBold(
            text: "Category",
            size: 20,
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            decoration: BoxDecoration(
              border: Border.all(width: 1.0, color: AppColors.mainTextColor),
              borderRadius: BorderRadius.circular(20),
            ),
            padding: const EdgeInsets.only(left: 10, right: 10),
            child: DropdownButtonHideUnderline(
              child: DropdownButton(
                hint: _dropDownValue == null
                    ? const Text('Select Category')
                    : Text(
                        _dropDownValue!,
                      ),
                isExpanded: true,
                iconSize: 24.0,
                items: ["Food & Drink", "Cake", "Tea"].map(
                  (val) {
                    return DropdownMenuItem<String>(
                      value: val,
                      child: Text(val),
                    );
                  },
                ).toList(),
                onChanged: (val) {
                  setState(
                    () {
                      _dropDownValue = val as String?;
                    },
                  );
                },
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _myRate() {
    return Container(
      width: double.maxFinite,
      padding: const EdgeInsets.only(left: 15, right: 15, top: 10, bottom: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextBold(
            text: "What your's rate?",
            size: 20,
          ),
          const SizedBox(
            height: 10,
          ),
          Wrap(
            children: List.generate(5, (index) {
              return Icon(
                Icons.star,
                color: AppColors.textColor2,
                size: 32,
              );
            }),
          ),
        ],
      ),
    );
  }

  Widget _myReviewTitle() {
    return Container(
      width: double.maxFinite,
      padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextBold(
            text: "Review Title",
            size: 20,
          ),
          const SizedBox(
            height: 10,
          ),
          const TextField(
            style: TextStyle(
              fontSize: 23.0,
            ),
            decoration: InputDecoration(
                border: InputBorder.none, hintText: "Enter the title"),
          ),
        ],
      ),
    );
  }

  Widget _myTitleRate_Share(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 50),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          TextBold(
            text: "Rate & Share",
            color: AppColors.buttonBackground,
          ),
          InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: SizedBox(
                child: TextNormal(
              text: "Cancel",
              color: AppColors.buttonBackground,
            )),
          ),
        ],
      ),
    );
  }
}
