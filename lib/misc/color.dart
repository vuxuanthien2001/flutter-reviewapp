import 'dart:ui';

class AppColors{
  static final Color textColor1= Color(0xFF989acd);
  static final Color textColor2 = Color(0xFF878593);
  static final Color bigTextColor=Color(0xFF2e2e31);
  static final Color mainColor= Color.fromARGB(255, 86, 106, 235);
    static final Color colorLocation= Color.fromARGB(255, 186, 9, 9);
    static final Color colorBlue =Color.fromARGB(255, 45, 148, 232);
  static final Color starColor = Color(0xFFe7bb4e);
   static final Color starColorRed = Color.fromARGB(255, 186, 9, 9);
  static final Color mainTextColor = Color(0xFFababad);
  static final Color mainTextColor2 = Color.fromARGB(255, 219, 219, 227);
  static final Color buttonBackground = Color(0xFFf1f1f9);
  static final Color colorCard1 = Color.fromARGB(255, 235, 235, 245);
  static final Color colorClickBottom = Color.fromRGBO(64, 47, 180, 1);
  static final Color colorUnClickBottom = Color.fromARGB(255, 158, 158, 163);
  static final Color colorWhile = Color.fromARGB(255, 255, 255, 255);
  static final Color colorWhile2 = Color.fromARGB(255, 222, 213, 213);


}