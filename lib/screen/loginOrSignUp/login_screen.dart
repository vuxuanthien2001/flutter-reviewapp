import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_appreview/misc/color.dart';
import 'package:flutter_appreview/screen/home_screen.dart';
import 'package:flutter_appreview/screen/loginOrSignUp/signup_screen.dart';
import 'package:flutter_appreview/widgets/text_bold.dart';
import 'package:flutter_appreview/widgets/text_normal.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../widgets/app_button.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            const _titleLogin(),
            const SizedBox(
              height: 50,
            ),
            Expanded(
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Container(
                  child: Column(
                    children: [
                      Container(
                        padding:
                            const EdgeInsets.only(left: 40, right: 40, top: 30),
                        child: Column(
                          // ignore: prefer_const_literals_to_create_immutables
                          children: [
                            const _inputEmail(),
                            const SizedBox(height: 20),
                            const _inputPassword(),
                            const SizedBox(height: 30),
                            const _buttonLogin(),
                            const _loginOther(),
                            const _signUp(),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _inputEmail extends StatelessWidget {
  const _inputEmail({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SizedBox(
      height: 50,
      child: TextField(
        decoration: InputDecoration(
          hintText: "Email",
          filled: true,
          prefixIcon: Icon(Icons.email),
          border: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.all(
              Radius.circular(15.0),
            ),
          ),
        ),
      ),
    );
  }
}

class _inputPassword extends StatelessWidget {
  const _inputPassword({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      child: TextField(
        obscureText: true,
        decoration: InputDecoration(
          hintText: "Password",
          filled: true,
          prefixIcon: const Icon(Icons.lock),
          suffixIcon: IconButton(
            icon: const Icon(Icons.visibility_off),
            onPressed: () {},
          ),
          border: const OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.all(
              Radius.circular(15.0),
            ),
          ),
        ),
      ),
    );
  }
}

class _buttonLogin extends StatelessWidget {
  const _buttonLogin({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: (() {
          Navigator.push(context,
              MaterialPageRoute(builder: ((context) => const HomePage())));
        }),
        child: ButtonApp(
          text: "LOGIN",
          textColor: AppColors.colorWhile,
          background: AppColors.mainColor,
          height: 40,
          textSize: 20,
        ));
  }
}

class _loginOther extends StatelessWidget {
  const _loginOther({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 10),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [TextNormal(text: "OR",)],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                  child: Container(
                padding: EdgeInsets.only(left: 50),
                child: SizedBox(
                  height: 32,
                  width: 32,
                  child: SvgPicture.asset(
                    "assets/icon/ic_facebook.svg",
                  ),
                ),
              )),
              Expanded(
                  child: Container(
                child: SizedBox(
                  height: 32,
                  width: 32,
                  child: SvgPicture.asset(
                    "assets/icon/google.svg",
                  ),
                ),
              )),
              Expanded(
                  child: Container(
                padding: EdgeInsets.only(right: 50),
                child: SizedBox(
                  height: 32,
                  width: 32,
                  child: SvgPicture.asset(
                    "assets/icon/twitter.svg",
                  ),
                ),
              )),
            ],
          ),
        ],
      ),
    );
  }
}

class _signUp extends StatelessWidget {
  const _signUp({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => const SignUpScreen()));
      },
      child: Container(
        padding: EdgeInsets.only(top: 20),
        child: Text.rich(TextSpan(
            text: "DON'T HAVE AN ACCOUNT?",
            style: const TextStyle(fontSize: 12),
            children: <InlineSpan>[
              TextSpan(
                text: " SIGN UP",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: AppColors.mainColor,
                    fontSize: 16),
              )
            ])),
      ),
    );
  }
}

class _titleLogin extends StatelessWidget {
  const _titleLogin({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      decoration: BoxDecoration(
        color: AppColors.mainColor,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(30),
          bottomRight: Radius.circular(30),
        ),
      ),
      padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
      child: Column(
        children: [
          Column(
            children: [
              const SizedBox(
                height: 150,
                width: 150,
                child: Image(image: AssetImage("assets/image/logo.png")),
              ),
              Text("LOGIN",
                  style: GoogleFonts.playfairDisplay(
                      fontSize: 32, color: AppColors.colorWhile)),
              const SizedBox(
                height: 20,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
