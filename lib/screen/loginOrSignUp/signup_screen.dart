import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_appreview/screen/loginOrSignUp/login_screen.dart';
import 'package:flutter_appreview/widgets/app_button.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../misc/color.dart';
import '../../widgets/text_bold.dart';

class SignUpScreen extends StatelessWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            _myLogo(),
            const SizedBox(
              height: 10,
            ),
            Expanded(
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Container(
                  child: Column(
                    children: [
                      Container(
                        padding:
                            const EdgeInsets.only(left: 20, right: 20, top: 30),
                        child: Column(
                          children: [
                            Container(
                              padding:
                                  const EdgeInsets.only(left: 20, right: 20),
                              child: Column(
                                // ignore: prefer_const_literals_to_create_immutables
                                children: [
                                  const _inputName(),
                                  const SizedBox(height: 20),
                                  const _inputEmail(),
                                  const SizedBox(height: 20),
                                  const _inputPassword(),
                                  const SizedBox(height: 30),
                                  const _buttobLogin(),
                                  const _login(),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _inputName extends StatelessWidget {
  const _inputName({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SizedBox(
      height: 50,
      child: TextField(
        decoration: InputDecoration(
          hintText: "Name",
          filled: true,
          prefixIcon: Icon(Icons.person),
          border: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.all(
              Radius.circular(15.0),
            ),
          ),
        ),
      ),
    );
  }
}

class _login extends StatelessWidget {
  const _login({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (() {
        Navigator.pop(context);
      }),
      child: Container(
        padding: const EdgeInsets.only(top: 20),
        child: Text.rich(TextSpan(
            text: "ALREADY HAVE AN ACCOUNT?",
            style: const TextStyle(fontSize: 12),
            children: <InlineSpan>[
              TextSpan(
                text: " LOGIN",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: AppColors.mainColor,
                    fontSize: 16),
              )
            ])),
      ),
    );
  }
}

class _buttobLogin extends StatelessWidget {
  const _buttobLogin({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ButtonApp(
        text: "SIGN UP",
        textColor: AppColors.colorWhile,
        background: AppColors.mainColor,
        height: 40,
        textSize: 20,);
  }
}

class _inputPassword extends StatelessWidget {
  const _inputPassword({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      child: TextField(
        obscureText: true,
        decoration: InputDecoration(
          hintText: "Password",
          filled: true,
          prefixIcon: const Icon(Icons.lock),
          suffixIcon: IconButton(
            icon: const Icon(Icons.visibility_off),
            onPressed: () {},
          ),
          border: const OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.all(
              Radius.circular(15.0),
            ),
          ),
        ),
      ),
    );
  }
}

class _inputEmail extends StatelessWidget {
  const _inputEmail({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SizedBox(
      height: 50,
      child: TextField(
        decoration: InputDecoration(
          hintText: "Email",
          filled: true,
          prefixIcon: Icon(Icons.email),
          border: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.all(
              Radius.circular(15.0),
            ),
          ),
        ),
      ),
    );
  }
}

class _myLogo extends StatelessWidget {
  const _myLogo({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      decoration: BoxDecoration(
        color: AppColors.mainColor,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(30),
          bottomRight: Radius.circular(30),
        ),
      ),
      padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
      child: Column(
        children: [
          Column(
            children: [
              const SizedBox(
                height: 150,
                width: 150,
                child: Image(image: AssetImage("assets/image/logo.png")),
              ),
              Text("SIGN UP",
                  style: GoogleFonts.playfairDisplay(
                      fontSize: 32, color: AppColors.colorWhile)),
              const SizedBox(
                height: 20,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
