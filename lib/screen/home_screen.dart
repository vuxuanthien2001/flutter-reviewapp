import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_appreview/misc/color.dart';
import 'package:flutter_appreview/pages/notifications_page.dart';
import 'package:flutter_appreview/pages/profile_page.dart';
import 'package:flutter_appreview/pages/reviews_page.dart';
import 'package:flutter_appreview/pages/search_page.dart';
import 'package:flutter_appreview/pages/rate_share.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List listPage = [
    const ReviewsPage(),
    const SearchPage(),
    const NotificationPage(),
    const ProfilePage()
  ];

  Widget currentScreen = ReviewsPage();

  final PageStorageBucket bucket = PageStorageBucket();

  int currentIndex = 0;
  void onTap(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageStorage(child: currentScreen, bucket: bucket),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        shape: const CircularNotchedRectangle(),
        child: Container(
          decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30), topRight: Radius.circular(30))),
          height: 60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  _myReviews(),
                  _mySearch(),
                ],
              ),
              _myRateShare(context),
              Row(
                children: [
                  _myNotification(),
                  _myProfile(),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _myRateShare(BuildContext context) {
    return SizedBox(
              height: 50,
              width: 50,
              child: FloatingActionButton(
                  // ignore: sort_child_properties_last
                  child: SvgPicture.asset(
                    "assets/icon/edit.svg",
                    color: Colors.white,
                  ),
                  backgroundColor: AppColors.mainColor,
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const RateSharePage(),
                      ),
                    );
                  }),
            );
  }

  Widget _myReviews() {
    return SizedBox(
                  height: 60,
                  width: 77,
                  child: MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        currentScreen = ReviewsPage();
                        currentIndex = 0;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 20,
                          width: 20,
                          child: SvgPicture.asset(
                            "assets/icon/reviews.svg",
                            color: currentIndex == 0
                                ? AppColors.colorClickBottom
                                : AppColors.colorUnClickBottom,
                          ),
                        ),
                        Text(
                          "Review",
                          style: TextStyle(
                            color: currentIndex == 0
                                ? AppColors.colorClickBottom
                                : AppColors.colorUnClickBottom,
                          ),
                        )
                      ],
                    ),
                  ),
                );
  }

  Widget _mySearch() {
    return SizedBox(
                  height: 60,
                  width: 80,
                  child: MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        currentScreen = const SearchPage();
                        currentIndex = 1;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 20,
                          width: 20,
                          child: SvgPicture.asset(
                            "assets/icon/find.svg",
                            color: currentIndex == 1
                                ? AppColors.colorClickBottom
                                : AppColors.colorUnClickBottom,
                          ),
                        ),
                        Text(
                          "Search",
                          style: TextStyle(
                            color: currentIndex == 1
                                ? AppColors.colorClickBottom
                                : AppColors.colorUnClickBottom,
                          ),
                        )
                      ],
                    ),
                  ),
                );
  }

  Widget _myNotification() {
    return SizedBox(
      height: 60,
      width: 80,
      child: MaterialButton(
        minWidth: 40,
        onPressed: () {
          setState(() {
            currentScreen = const NotificationPage();
            currentIndex = 2;
          });
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 20,
              width: 20,
              child: SvgPicture.asset(
                "assets/icon/bell.svg",
                color: currentIndex == 2
                    ? AppColors.colorClickBottom
                    : AppColors.colorUnClickBottom,
              ),
            ),
            Text(
              "Notification",
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: currentIndex == 2
                    ? AppColors.colorClickBottom
                    : AppColors.colorUnClickBottom,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _myProfile() {
    return SizedBox(
      height: 60,
      width: 71,
      child: MaterialButton(
        minWidth: 40,
        onPressed: () {
          setState(() {
            currentScreen = ProfilePage();
            currentIndex = 3;
          });
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 20,
              width: 20,
              child: SvgPicture.asset(
                "assets/icon/user.svg",
                color: currentIndex == 3
                    ? AppColors.colorClickBottom
                    : AppColors.colorUnClickBottom,
              ),
            ),
            Text(
              "Profile",
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: currentIndex == 3
                    ? AppColors.colorClickBottom
                    : AppColors.colorUnClickBottom,
              ),
            )
          ],
        ),
      ),
    );
  }
}
