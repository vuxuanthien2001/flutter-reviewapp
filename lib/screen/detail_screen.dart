import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_appreview/misc/color.dart';
import 'package:flutter_appreview/widgets/app_icon.dart';
import 'package:flutter_appreview/widgets/text_bold.dart';
import 'package:flutter_appreview/widgets/text_normal.dart';
import 'package:flutter_svg/svg.dart';

class DetailPage extends StatefulWidget {
  const DetailPage({Key? key}) : super(key: key);

  @override
  State<DetailPage> createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: AppColors.mainColor,
        child: Column(
          children: [
            const SizedBox(height: 20,),
            Container(
              padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
              child: _myTitle(context),
            ),
            const SizedBox(
              height: 10,
            ),
            Expanded(
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Container(
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30),
                    ),
                  ),
                  child: Column(
                    children: [
                      _myPostReview(),
                      _myImageReview(),
                      _myCategory_Location(),
                      _myFeeling(),
                      _myListComment()
                    ],
                  ),
                ),
              ),
            ),
            _myComment()
          ],
        ),
      ),
    );
  }

  Widget _myComment() {
    return Container(
              height: 80,
              padding: const EdgeInsets.only(left: 30, right: 30, bottom: 10),
              decoration:
                  BoxDecoration(color: AppColors.colorWhile2, boxShadow: [
                BoxShadow(
                    offset: const Offset(0, 4),
                    blurRadius: 32,
                    color: AppColors.mainTextColor)
              ]),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: 50,
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    decoration: BoxDecoration(
                        color: AppColors.colorWhile,
                        borderRadius: BorderRadius.circular(30)),
                    width: double.maxFinite,
                    child: Row(
                      children: [
                        const Expanded(
                          child: SizedBox(
                            child: TextField(
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: "Write comment here ..."),
                            ),
                          ),
                        ),
                        CircleAvatar(
                          backgroundColor: AppColors.mainColor,
                          
                          child: AppIcon(source: "assets/icon/send.svg", color: AppColors.colorWhile, height: 18, width: 18,)
                        )
                      ],
                    ),
                  )
                ],
              ));
  }

  Widget _myListComment() {
    return ListView.builder(
                      shrinkWrap: true,
                      primary: false,
                      itemCount: 3,
                      itemBuilder: (context, index) {
                        return Column(
                          children: [
                            Container(
                              padding: const EdgeInsets.only(
                                  left: 15, right: 15, top: 5),
                              color: AppColors.mainTextColor2,
                              child: Column(
                                children: [
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    // ignore: prefer_const_literals_to_create_immutables
                                    children: [
                                      const SizedBox(
                                          height: 50,
                                          width: 50,
                                          child: CircleAvatar(
                                            backgroundImage: NetworkImage(
                                                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4KLQr4UnxPHkhfpKkU6bY_04vGtGzTKB_pg&usqp=CAU"),
                                          )),
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      Card(
                                        shape: const RoundedRectangleBorder(
                                            borderRadius: BorderRadius.only(
                                                topRight: Radius.circular(10),
                                                bottomLeft:
                                                    Radius.circular(10),
                                                bottomRight:
                                                    Radius.circular(10))),
                                        color: AppColors.colorWhile,
                                        child: Container(
                                          padding: const EdgeInsets.only(
                                              top: 5, left: 10, right: 10),
                                          width: 260,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              TextBold(
                                                text: "Douglas Schnei",
                                                size: 15,
                                              ),
                                              const SizedBox(
                                                height: 5,
                                              ),
                                              TextNormal(
                                                  text:
                                                      "Thanks for the information. Your review help me very much :D"),
                                              const SizedBox(height: 8,),
                                              const Text.rich(TextSpan(
                                                  text: "3m ago",
                                                  style: TextStyle(
                                                      color: Colors.grey),
                                                  children: <InlineSpan>[
                                                    TextSpan(
                                                      text: "   Like   Relpy",
                                                      style: TextStyle(
                                                          color: Colors.grey,
                                                          fontWeight:
                                                              FontWeight
                                                                  .bold),
                                                    )
                                                  ])),
                                              const SizedBox(
                                                height: 5,
                                              ),
                                              Row(
                                                children: [
                                                  const SizedBox(
                                                    height: 30,
                                                    width: 30,
                                                    child: CircleAvatar(
                                                        backgroundImage:
                                                            NetworkImage(
                                                                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTAm0_gOTq0-Xp24EVWDyrq6V3RQTMTD7etdA&usqp=CAU")),
                                                  ),
                                                  const SizedBox(
                                                    width: 5,
                                                  ),
                                                  TextBold(
                                                    text: "Eleanor Waters",
                                                    size: 15,
                                                  ),
                                                  const SizedBox(
                                                    width: 5,
                                                  ),
                                                  TextNormal(
                                                    text:
                                                        "I aggreed with you",
                                                    size: 12,
                                                  )
                                                ],
                                              ),
                                              const SizedBox(
                                                height: 5,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            )
                          ],
                        );
                      },
                    );
  }

  Widget _myFeeling() {
    return Container(
                      padding: const EdgeInsets.only(top: 15, bottom: 10),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              padding: const EdgeInsets.only(right: 20),
                              child: Column(
                                children: [
                                  
                                  AppIcon(source: "assets/icon/like.svg", color: AppColors.mainTextColor),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  TextNormal(
                                    text: "68",
                                    color: AppColors.mainTextColor,
                                  )
                                ],
                              ),
                            ),
                            Container(
                              padding:
                                  const EdgeInsets.only(left: 20, right: 20),
                              child: Column(
                                children: [
                                  
                                  AppIcon(source: "assets/icon/dislike.svg", color: AppColors.mainTextColor),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  TextNormal(
                                    text: "0",
                                    color: AppColors.mainTextColor,
                                  )
                                ],
                              ),
                            ),
                            Container(
                              padding:
                                  const EdgeInsets.only(left: 20, right: 20),
                              child: Column(
                                children: [
                                  
                                  AppIcon(source: "assets/icon/chat.svg", color: AppColors.mainTextColor),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  TextNormal(
                                    text: "8",
                                    color: AppColors.mainTextColor,
                                  )
                                ],
                              ),
                            ),
                            Container(
                              padding: const EdgeInsets.only(left: 20),
                              child: Column(
                                children: [
                                  
                                  AppIcon(source: "assets/icon/share-option.svg", color: AppColors.mainTextColor),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  TextNormal(
                                    text: "3",
                                    color: AppColors.mainTextColor,
                                  )
                                ],
                              ),
                            ),
                          ]),
                    );
  }

  Widget _myCategory_Location() {
    return Container(
                      height: 60,
                      margin: const EdgeInsets.only(left: 15, right: 15),
                      decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10)),
                        color: AppColors.colorCard1,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            padding: const EdgeInsets.only(left: 20, top: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                TextBold(
                                  text: "Category",
                                  size: 12,
                                  color: AppColors.textColor2,
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                TextNormal(
                                  text: "Food & Drink",
                                  color: AppColors.mainTextColor,
                                  size: 18,
                                )
                              ],
                            ),
                          ),
                          Container(
                            padding:
                                const EdgeInsets.only(right: 20, top: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                TextBold(
                                  text: "Location",
                                  size: 12,
                                  color: AppColors.textColor2,
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                TextNormal(
                                  text: "Secang Yogyakarta",
                                  color: AppColors.mainTextColor,
                                  size: 18,
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    );
  }

  Widget _myImageReview() {
    return Container(
                      padding: const EdgeInsets.only(
                          top: 10, bottom: 10, left: 15, right: 15),
                      height: 150,
                      child: Row(
                        children: [
                          Container(
                            width: 200,
                            height: 150,
                            decoration: const BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              image: DecorationImage(
                                  image: NetworkImage(
                                      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSYgv_7WcHGDMSb2j_ZbjdqXWr9s0UEumDMag&usqp=CAU"),
                                  fit: BoxFit.cover),
                            ),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Stack(
                            children: [
                              Positioned(
                                  child: Container(
                                width: 120,
                                height: 150,
                                decoration: const BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10)),
                                  image: DecorationImage(
                                      image: NetworkImage(
                                          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR-Gcu0mBkr0uW_OPxSC_nlpdH2W7uTpqcZ6A&usqp=CAU"),
                                      fit: BoxFit.cover),
                                ),
                              )),
                              Positioned(
                                  child: Container(
                                      width: 120,
                                      height: 150,
                                      decoration: BoxDecoration(
                                        borderRadius: const BorderRadius.all(
                                            Radius.circular(10)),
                                        color:
                                            const Color.fromARGB(255, 0, 0, 0)
                                                .withOpacity(0.5),
                                      ),
                                      child: Container(
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            TextBold(
                                              text: "6",
                                              color:
                                                  AppColors.buttonBackground,
                                            ),
                                            TextNormal(
                                                text: "more",
                                                color: AppColors
                                                    .buttonBackground)
                                          ],
                                        ),
                                      ))),
                            ],
                          ),
                        ],
                      ),
                    );
  }

  Widget _myPostReview() {
    return Container(
                      padding:
                          const EdgeInsets.only(left: 15, right: 15, top: 10),
                      child: Column(
                        children: [
                          TextBold(text: "Secangkir Jawa", size: 20,),
                          const SizedBox(
                            height: 5,
                          ),
                          Wrap(
                            children: List.generate(5, (index) {
                              return Icon(Icons.star, size: 32,
                                  color: AppColors.starColor);
                            }),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          TextNormal(
                            text:
                                "Yesterday, I went to Burger King in the town with my husband and my child. I will coming back next week, very recommend",
                            color: AppColors.bigTextColor,
                          ),
                        ],
                      ),
                    );
  }

  Widget _myTitle(BuildContext context) {
    return Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                IconButton(
                  iconSize: 33,
                  icon: Icon(
                    Icons.chevron_left,
                    color: AppColors.buttonBackground,
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
                const SizedBox(
                  height: 50,
                  width: 50,
                  child: CircleAvatar(
                    backgroundImage: NetworkImage(
                        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR4n0ddX3HxaKRtCaF-kOHrOALtmjJgc4--qA&usqp=CAU"),
                  ),
                ),
                Expanded(
                    child: Container(
                  child: Row(
                    children: [
                      const SizedBox(
                        width: 8,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextBold(
                            text: "Carolyne Agustine",
                            size: 16,
                            color: AppColors.buttonBackground,
                          ),
                          TextNormal(
                            text: "25 minutes ago",
                            color: AppColors.mainTextColor,
                          )
                        ],
                      ),
                    ],
                  ),
                )),
                SizedBox(
                  height: 30,
                  width: 30,
                  child: SvgPicture.asset("assets/icon/more.svg",
                      color: AppColors.mainTextColor),
                ),
              ],
            );
  }
}
