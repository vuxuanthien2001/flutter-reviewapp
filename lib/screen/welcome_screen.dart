import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_appreview/screen/home_screen.dart';
import 'package:flutter_appreview/screen/loginOrSignUp/login_screen.dart';
import 'package:flutter_appreview/widgets/text_bold.dart';
import 'package:flutter_appreview/widgets/text_normal.dart';
import 'package:flutter_svg/svg.dart';

import '../misc/color.dart';
import '../widgets/app_icon.dart';

class WelComePage extends StatelessWidget {
  const WelComePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/image/backgroundreviewapp.jpg"),
                fit: BoxFit.fill)),
        padding: EdgeInsets.only(left: 20, top: 50, right: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          // ignore: prefer_const_literals_to_create_immutables
          children: [
            const _name_logo(),
            const SizedBox(
              height: 20,
            ),
            const _slogan(),
            const SizedBox(height: 10,),
            const _listTopic(),
            const _myWelcome(),
            const _skipLogin(),
          ],
        ),
      ),
    );
  }
}

class _skipLogin extends StatelessWidget {
  const _skipLogin({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 100),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const LoginScreen(),
                ),
              );
            },
            child: Container(
                width: 100,
                padding: const EdgeInsets.only(bottom: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    TextNormal(
                      text: "Skip",
                      color: Theme.of(context)
                          .textTheme
                          .bodyText1!
                          .color!
                          .withOpacity(0.64),
                    ),
                    Icon(
                      Icons.arrow_forward_ios,
                      color: Theme.of(context)
                          .textTheme
                          .bodyText1!
                          .color!
                          .withOpacity(0.64),
                    )
                  ],
                )),
          ),
        ],
      ),
    );
  }
}

class _myWelcome extends StatelessWidget {
  const _myWelcome({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            padding: const EdgeInsets.only(
              top: 80,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Wrap(
                  children: List.generate(5, (index) {
                    return Icon(
                      Icons.star,
                      color: AppColors.starColorRed,
                      size: 42,
                    );
                  }),
                ),
              ],
            ),
          ),
          TextBold(text: "Welcome")
        ],
      ),
    );
  }
}

class _listTopic extends StatelessWidget {
  const _listTopic({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.only(top: 50),
          child: Row(
            children: [
              Expanded(
                child: Column(
                  children: [
                    SizedBox(
                      height: 32,
                      width: 32,
                      child: SvgPicture.asset(
                        "assets/icon/foodanddrink.svg",
                      ),
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    TextNormal(text: "Food & drink"),
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  children: [
                    SizedBox(
                      height: 32,
                      width: 32,
                      child: SvgPicture.asset(
                        "assets/icon/travel.svg",
                      ),
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    TextNormal(text: "Travel"),
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  children: [
                    SizedBox(
                      height: 32,
                      width: 32,
                      child: SvgPicture.asset(
                        "assets/icon/shopping.svg",
                      ),
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    TextNormal(text: "Shopping"),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          padding: const EdgeInsets.only(top: 20),
          child: Row(
            children: [
              Expanded(
                child: Column(
                  children: [
                    SizedBox(
                      height: 32,
                      width: 32,
                      child: SvgPicture.asset(
                        "assets/icon/maintenance.svg",
                      ),
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    TextNormal(text: "Auto Repair"),
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  children: [
                    SizedBox(
                      height: 32,
                      width: 32,
                      child: SvgPicture.asset(
                        "assets/icon/homeservices.svg",
                      ),
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    TextNormal(text: "Home Services"),
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  children: [
                    SizedBox(
                      height: 32,
                      width: 32,
                      child: SvgPicture.asset(
                        "assets/icon/more.svg",
                      ),
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    TextNormal(text: "+ More"),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class _slogan extends StatelessWidget {
  const _slogan({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: TextBold(
      text: "Find top rated \nbusinesses for\nevery need",
      size: 24,
    ));
  }
}

class _name_logo extends StatelessWidget {
  const _name_logo({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        TextBold(
          text: "SUPERREVIEW",
          size: 24,
        ),
        const SizedBox(
          height: 50,
          width: 50,
          child: Image(image: AssetImage("assets/image/logo.png")),
        )
      ],
    );
  }
}
